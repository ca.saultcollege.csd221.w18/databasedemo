/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.saultcollege.csd221.w18.databasedemo.JPA;

import java.util.List;
import javax.persistence.*;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Root;

/**
 *
 * @author rod
 */
public class Main {
    
    public static void main(String[] args) {
        EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("bookstore");
        EntityManager em = emfactory.createEntityManager();
        em.getTransaction().begin();
        
//        Author author = em.find(Author.class, 3);
//        
//        for ( Book b : author.getBooks() ) {
//            System.out.println(b);
//        }
        
//        printBooksInCategory(em, "Classic");

        Author a = new Author();
        a.setFirstName("Douglas");
        a.setLastName("Adams");
        em.persist(a);
        
        Book b = new Book();
        b.setTitle("The Hitchiker's Guide to the Galaxy");
        b.setAuthor(a);
        em.persist(b);
              
        em.getTransaction().commit();
        em.close();
        emfactory.close();

    }
    
    public static void printBooksInCategory(EntityManager em, String categoryName ) {
        TypedQuery<Book> q = em.createQuery(
                "SELECT b FROM Book b "
                + "JOIN b.categories cat "
                + "WHERE cat.name = :catname",
                Book.class
        );
        
        q.setParameter("catname", categoryName);
        
        q.getResultStream().forEach(System.out::println);
        
    }
    
}
